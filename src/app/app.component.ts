import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Countries } from './interfaces/country';
import * as COUNTRIES from './data/countries.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  data: Countries = COUNTRIES;
  todayDate = new Date().toLocaleDateString('en-ca');
  passwordPattern!: string;
  selectedCountry!: number;

  ngOnInit(): void {}

  log(message: NgModel): void {
    console.log(message);
  }

  validateNumbers(e: KeyboardEvent): void {
    if (!Number.isInteger(parseInt(e.key))) {
      e.preventDefault();
    }
  }

  submit(form: NgForm) {
    console.log(form.value);
  }
}
